---
layout: layouts/default.html
title: Picadillo
tags: meals
---

# Picadillo
## Ingredients
* 3 lbs ground meat: turkey, chicken, beef
* 2 guajillo chilis, if not 2 california chilis 
* 2 tomatoes
* 2 potatoes
* 3 carrots
* 1 squash
* 2 garlic cloves


## Directions
1. Cut all of the veggies into small cubes. 
1. Remove chili seeds and boil chilis in a pot with 2 tomatoes for 10 minutes. When it is done, season with salt, pepper, oregano, garlic, a pinch of chicken buillon (optional). Blend together. 
2. In a seperate pan, cook meat until no longer red (5-10 min). After done, season with salt and pepper.
3. In a small pan, fry potatoes until tender for about 8 minutes. Then, fry squash until tender for about 8 minutes.
4. Boil carrots for 5 minutes.
5. When the meat is done, add blended chili sauce and all the veggies together. let it cook together for 5 minutes. 