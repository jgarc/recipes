---
layout: layouts/default.html
title: Keema Curry
tags: meals
---
# Keema Curry 

Typically 3x the amount for meal prep and sharing with bby. [Recipe source](https://www.justonecookbook.com/keema-curry/)

## Ingredients
- 1 onion (7 oz, 200 g)
- 1 stalk celery (2 oz, 57 g)
- ½ carrot (3.5 oz, 100 g)
- 6 [shiitake mushrooms](https://www.justonecookbook.com/shiitake-mushrooms/) (1.7 oz, 48 g; You can use [dried shiitake mushrooms](https://www.justonecookbook.com/dried-shiitake-mushroom/) instead of fresh ones. If using dried shiitake mushrooms, soak them in 1 cup of water for 15 minutes. Squeeze the water out from the mushrooms and use this liquid in place of water)
- 1 Tbsp neutral-flavored oil (vegetable, rice bran, canola, etc.)
- 1 lb ground pork (You can also use beef or chicken; for vegan/vegetarian, use mushrooms, zucchini, eggplant, tofu, etc)
- ¼ tsp kosher salt (Diamond Crystal; use half for table salt)
- freshly ground black pepper

### Seasoning
- 1 cup [Chicken Stock/Broth (homemade or store bought)](https://www.justonecookbook.com/homemade-chicken-stock/) (You can use vegetable stock for vegetarian/vegan)
- ½ cup water (or more)
- 1 tsp [Japanese curry powder](https://www.justonecookbook.com/japanese-curry-powder/)
- 1 Tbsp unsalted butter
- 2 cubes [Japanese curry roux](https://www.justonecookbook.com/japanese-curry-sauce-mix-roux/) (Roughly 2 oz or 50 g. You can make [homemade Japanese curry roux](https://www.justonecookbook.com/how-to-make-curry-roux/).)
- 1 Tbsp ketchup
- 1 Tbsp [Tonkatsu sauce](https://www.justonecookbook.com/tonkatsu-sauce/)
  
## Steps
1. Gather all the ingredients. Prepare steamed rice first, if you don't have any.
2. Chop the onion finely. Mince celery, carrots, mushrooms.
3. In a large skillet, heat oil over medium heat. Sauté the onion until translucent.
4. Add ground pork and cook until no longer pink. Season with salt and pepper.
5. Add celery, carrots, and shiitake mushrooms and mix well with the rest of the ingredients.
6. Add chicken stock and water. Add more water, if necessary, so the cooking liquid covers the ingredients.
7. Add curry powder and mix well. Cover and bring it to boil. Skim off the scum and foam on the surface with a fine-mesh skimmer as necessary. Reduce heat to medium-low heat and cook covered until the vegetables are tender, about 6-8 minutes.
8. Add curry roux (one piece at a time!) and butter and let them dissolved completely. The curry will thicken as it's heated up.
9. Add ketchup and Tonkatsu sauce. Mix well and simmer for 3-5 minutes.
