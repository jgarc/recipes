---
layout: layouts/default.html
title: Refried Beans
tags: sides
---
# Refried beans

## Ingredients
* Pinto beans
* Soy chorizo
* handful of chiles del arbol
* cooking oil of choice, preferably lard


1. Presoak beans for a couple of hours if possible. 
2. Boil beans in hot water until fully cooked. 40 mins - 2 hours. 
3. Add some oil with ~3 chiles del arbol and add soyrizo ~1/3 of the package. 
