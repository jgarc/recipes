---
layout: layouts/default.html
title: Chai Harder
tags: drinks
---
# Chai Harder

* 1.5 oz chai infused tequila 
* .5 oz agave syrup or Xila mezcal liqueur
* .75 oz honey ginger syrup
* .75 oz lemon juice

Shake on ice (or stir until cold) strain into glass
The honey syrup separates so make sure you shake it before pouring

