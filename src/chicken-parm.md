---
layout: layouts/default.html
title: Crispy Chicken Parmesan
tags: meals
---
![](../img/chicken-parm.jpg)

# Crispy Chicken Parmesan

Makes 6 servings

* 2 large eggs
* 1 tablespoon minced garlic
* 2 tablespoons fresh chopped parsley
* Salt and pepper to season
* 3 large chicken breasts halved horizontally to make 6 fillets
* 1 cup Panko breadcrumbs
* 5/6 cup fresh grated parmesan cheese (1/2 for chicken, rest for serving)
* 1 teaspoon garlic or onion powder
* 1/2 cup olive oil for frying
* Any premade sauce
* 8 ounces (250 g) mozzarella cheese sliced or shredded


1. Whisk together eggs, garlic, parsley, salt and pepper in a shallow dish. Add chicken into the egg, rotating to evenly coat each fillet in the mixture. Cover with plastic wrap and allow to marinate for at least 15 minutes (or overnight night if time allows for a deeper flavour).
1. When chicken is ready for cooking, mix bread crumbs, Parmesan cheese and garlic powder together in a separate shallow bowl. Dip chicken into the breadcrumb mixture to evenly coat.
1. Preheat oven 430°F | 220°C. Lightly grease an oven tray (or baking dish) with non stick cooking oil spray; set aside.
1. Heat oil in a large skillet over medium-high heat until hot and shimmering. Fry chicken until golden and crispy, (about 4-5 minutes each side).
Place chicken on prepared baking tray / dish and top each breast with about 1/3 cup of sauce (sauce recipe below). Top each chicken breast with 2-3 slices of mozzarella cheese and about 2 tablespoons parmesan cheese. Sprinkle with basil or parsley.
1. Bake for 15-20 minutes, or until cheese is bubbling and melted, and the chicken is completely cooked through.
