---
layout: layouts/default.html
title: Barista Workflow
tags: coffee
---

# Barista Workflow

Great Starting Reference: [https://espressoaf.com/](https://espressoaf.com/)

The machine I have is a [Rancilio Silvia V4](https://espressoaf.com/recommendations/entry-machines#rancillo-silvia-900), a solid entry level machine. I use a [1zpresso J-Ultra hand grinder](https://1zpresso.coffee/j-ultra/) which is made specifically for grinding espresso. While hand-grinding is a pain, I would need 2-3x my budget to get a similar quality electric machine. 

Ideal espresso should be brewed within 3 minutes of grinding, with roughly 1-2 week old whole beans. [As a starting point](https://espressoaf.com/guides/beginner.html#2-quick-grind-size-adjustment), aim for a 25–35 second shot with a 1:2 ratio(example: 14g in → 28g out). You can get really nerdy by [measuring the density](https://density.coffee/form/) of your coffee beans to get a *better* starting point. 

> Note: I have a naked portafilter, and one with two spouts. If you want to share drinks, the two spouts can help distribute two single shots. Note that if your prep is uneven, so will the shots! 

## Espresso

1. Turn on machine with right switch 10-15 minutes before extraction to stabalize temperature. Once light is off, machine is ready for use. 
1. Remove the portafilter from grouphead and purge some water.
1. Wipe Basket Clean and dry with the left rag on top of the machine. 
1. I have a 15g and a 20g basket (smaller and larger basket). You should dose for the rated capacity ± 1 gram. 
1. Use dosing funnel and [WDT](https://espressoaf.com/guides/puckprep.html#weiss-distribution-technique-wdt) and circular-wedge distribution tool. 
1. The tamp is weighted to 9 lbs of pressure. Tamp level and in a single motion. The tamp should stop when the appropriate amount of force is exterted. 
1. Recommended: insert screen. Helps to keep grouphead clean. 
1. Tare your cup with scale.  Lock portafilter into the machine. Bring scale and cup below machine. Start when ready. 
1. Serve and taste. [Adjust to taste as necessary.](https://espressoaf.com/guides/beginner.html#3-adjust-brew-ratio-to-taste).
1. Remove grounds. With screen present, you can tap on the side to dislodge screen while keeping puck steady. In swift motion, shake portafilter downward over trash to dislodge coffee puck. 
1. Wipe basket dry.
1. Purge grouphead and return portafilter to locked position.
1. With rag not on the machine, wipe clean any area dirtied during drink preparation.  

## Steaming milk

https://espressoaf.com/guides/latteart.html#milk-steaming

Turn on bottom left switch. You will need to wait for milk to steam as there is a single boiler in the machine. Release steam by turning right knot to the left. When using steam wand, **only touch from the rubber part as the wand is very hot**. After done, the only ask I have is fill the boiler with water after releasing steam on the machine. You can do this by trying to run water from the grouphead until the light turns on and same water comes out from the grouphead. 


## Next Steps
* [Temperature surfing](https://www.seattlecoffeegear.com/blogs/scg-blog/tech-tips-temperature-surfing-rancilio-silvia)
* [Water preperation](https://espressoaf.com/guides/water.html)

