---
layout: layouts/default.html
title: Rice Cooker Jambalaya
tags: Instant Pot
---

## Ingredients

* 1 pound andouille sausage, (any fully cooked sausage would be great) kind - cut lengthwise and then into 1/4 inch half circles
* 1 pound raw shrimp, peeled and de-veined (31/40's are perfect)
* 2 cups cooked chicken, shredded into bite sized pieces
* 1 1/2 cups uncooked rice
* 4 cups chicken broth
* One -7oz can diced green chilies
* 1-2 tablespoons Creole seasoning
* Parsley to stir in at the end

## Directions

1. Put everything into the pot, stir well, close and lock and turn to sealing position
1. Select rice setting / high pressure and set the timer to 8 minutes
1. When the time is up, quick release, remove the cover, and the pot will switch to thekeep warm setting
1. Stir in parsley
