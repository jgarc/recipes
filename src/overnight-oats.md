---
layout: layouts/default.html
title: Overnight Oats
tags: meals
---

# Overnight Oats

* 90g – Oatmeal 
* 175g – Fatfree Yogurt 
* 250ml – Nut Milk of Choice
* 1 Scoop – Whey Protein Powder
* ½ cup – Fruit of choice (bananas, blueberries, apples)

Optionally 16g – Powdered Peanut butter
