---
layout: layouts/default.html
title: Vegan Flatbread
tags: sides
---

# Vegan flatbread

* 1½ cups all purpose flour
* 1½ teaspoons baking powder
* 1 teaspoon salt
* 2 tablespoons olive oil
* ½ cup unsweetened non-dairy milk or water

# Instructions

1. Whisk the flour, baking powder, and salt together in a large mixing bowl.
1. Whisk the olive oil and non-dairy milk or water together in a small bowl.
1. Slowly pour the wet ingredients into the dry ingredients while stirring. When the dough becomes too thick to stir, use your hands to form it into a soft ball.  If the dough is too sticky, add a bit more flour (I added another tablespoon). If it’s crumbling and not holding together, add another teaspoon of water.
1. Place the dough in the bowl and cover with a kitchen towel. Let it rest for 10 minutes.
1. Divide the dough into four equal pieces and roll into balls. Use a rolling pin or the palm of your hand to flatten each ball into 6-inch diameter rounds, about ¼-inch thick.
1. Pour a teaspoon of olive oil into a skillet and warm over medium low heat. Working one at a time, cook the flatbreads for 3 to 4 minutes on each side, until 1. golden brown, adding more olive oil to the pan as needed. 
1. Enjoy the flatbreads warm or cool them to room temperature and store in the fridge or freezer.


# Notes

* Store leftover flatbreads in the freezer for several months or in the fridge for up to a week.
* Nutrition info was created using Oatly oat milk.