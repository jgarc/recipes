---
layout: layouts/default.html
title: 10 Minute (Truffle) Mac & Cheese
tags: Instant Pot
---

## Ingredients

* 1 pound uncooked pasta, could literally be any pasta – I went old school with elbow  
* 4 cups chicken broth or stock
* 1 teaspoon garlic powder
* 1 teaspoon onion powder
* 1 cup heavy cream
* 1 cup grated Parmesan
* 1 cup grated smoked Gouda
* 1 cup shredded white cheddar
* 2 teaspoons truffle oil, optional
* 1-2 teaspoons kosher salt
* Optional: Diced green chilies or jalapenos, cooked crispy bacon, chunks of blue cheese, etc

## Steps

1. Put the pasta, broth and garlic & onion powders into your Instant Pot
1. Lock,turn to sealing position, select high pressure and cook 4 minutes, then quick release the steam (you might want to put a towel over the steam cuz pasta water vapor can be messy)
1. Add the cream (start with ¾ and add more if you want it creamier), the cheeses,truffle oil if using and salt to taste
1. Stir super well until beautifully mixed – and if you’re adding anything else, this is the time
