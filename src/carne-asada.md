---
layout: layouts/default.html
title: Carne Asada
tags: meals
---
# Carne Asada

* 2 lb flank steak
* 4 Clove's garlic minced
* 1/2 cup cilantro
* 1 lime zest
* Juice of one lime
* Juice of one orange 
* Teaspoon of cumin 
* Salt and pepper to taste
* 1/2 minced onion
* 3/4 cup olive oil 
* A teaspoon of apple cider vinegar

1. Mix marinade
1. Steak in marinade 90mins - up to 4hours
1. Grill on medium-high heat 4-6 mins per side


