---
layout: layouts/default.html
title: Green Smoothie
tags: meals
---

# Green Smoothie

* 250 ml – Coconut water
* Juice of 1 lime
* 100g - Pineapple
* 175g - Greek Yogurt
* 40g - Celery
* 75g - Spinach
* An Apple
* 75g Cucumber 