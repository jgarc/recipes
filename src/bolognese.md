---
layout: layouts/default.html
title: Instant Pot Bolognese
tags: Instant Pot
---

# Instant Pot Bolognese

**Yield:** 6

A Bolognese is the ultimate meat sauce simmered in red wine to create an unforgettably divine flavor. I like mine with a touch of cream.

## Times
- **Prep Time:** 5 minutes
- **Cook Time:** 22 minutes
- **Total Time:** 27 minutes

## Ingredients
- 1/4 cup of extra virgin olive oil
- 1 large Spanish onion, diced
- 1 large carrot, peeled and diced
- 2 stalks of celery, diced (reserve the leaves/leafy tops from the stalk if possible)
- 2 lbs of a ground meat of your choice (Turkey, Chicken, Beef, etc)
- 1 tbsp of crushed or minced garlic
- 3/4 cup of dry red wine
- 1/4 cup of dry white wine
- 28 oz can of crushed tomatoes (use San Marzano if you can swing it – worth the difference)
- 2 cups of beef broth (We use 2 tsp of [Beef Better Than Bouillon](https://amzn.to/2S4Kmqn) + 2 cups of water)
- 1 tsp of kosher salt
- 1 tsp of seasoned salt
- 1 tsp of Italian seasoning
- 1 tsp of oregano
- 1/4 tsp of nutmeg (optional)
- 1 lb (1 box) of Ziti Rigati (like a Ziti with ridges)
- 1/2 cup of heavy cream or half/half (optional)
- 5.2 oz of Boursin or 5 oz of a brick of cream cheese (optional, for an even richer experience)
- Parmesan cheese, for topping

## Instructions
1. Add the olive oil to the Instant Pot  and then hit “Sauté” and adjust so it’s on the “More” or “High” setting. Allow it to heat up for three minutes and then add in the onion, carrot, and celery. Sauté for 5 minutes and then add in the garlic and sauté for 1 minute longer.
2. Next, add in the ground meat, stir, and allow it to sauté for 5 minutes with the vegetables until it crumbles and releases its juices (which we will keep in as it adds a ton of delicious flavor).
3. Pour in the red and white wine, stir it in with the meat and vegetables, and allow the meat and veggies to simmer in it for 10 solid minutes (do not go less than 10 minutes as this is where the rich flavor is born into the meat as well as the alcohol burning off).
4. Next, add in the crushed tomatoes, broth, seasoned salt, kosher salt, nutmeg, oregano, and Italian seasoning. Stir everything together very well.
5. Lastly, add in the pasta **BUT DO NOT STIR** (or you may have issues coming to pressure). Simply press and smooth the pasta down with a spoon so it’s submerged in the broth (it’s okay if it peaks above a little).
6. Secure the lid and hit “Keep Warm/Cancel” and then hit “Manual” or “Pressure Cook” for 6 minutes at High Pressure (this cook time is for Ziti Rigati. If using Rigatoni, go for 8 minutes). Quick release when done and give everything a good stir.
7. Finish it off by adding in the cream and the Boursin (or cream cheese) and stirring for another minute or two until totally melded into the sauce **-OR-** if you prefer it without the dairy and like it with just a meat-and-tomato base, simply leave out the cream and Boursin and serve as is!
8. Serve in bowls and top with some Parmesan cheese, if desired.
9. Mangia!

## Jeffrey's Tips
- Sautéing the meat in wine is what truly makes a Bolognese a Bolognese. But if you can’t tolerate it, substitute the wine with additional beef broth and simmer the meat in 1 cup of it in place of the wine for 10 minutes before adding the other 2 cups with the crushed tomatoes prior to pressure cooking.
- As mentioned, the cream and Boursin/cream cheese is optional but I think it makes it the greatest and most flavorful Bolognese out there! It doesn’t over-cream the sauce at all, but rather gives it a wonderful hint of additional flavor and color while allowing the meat to remain the star of the show.

## Video
[Watch on YouTube](https://www.youtube.com/watch?v=5BB9WxHBJ6I)

[Original Recipe](https://pressureluckcooking.com/instant-pot-pasta-bolognese/)
